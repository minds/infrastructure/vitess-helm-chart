# Minds Vitess Cluster

!! Note: this repo is being deprecated in favor of the OKE module: https://gitlab.com/minds/infrastructure/modules/vitess

!! Note: this module assumes the vitess operator is already installed. You can install the operator by running `kubectl apply -f vitess-operator.yaml`

## Topology

Our Vitess cluster is deployed with 6 cells. This includes one per [AWS availability zone](https://docs.aws.amazon.com/AWSEC2/latest/UserGuide/using-regions-availability-zones.html#concepts-availability-zones) in which our EKS nodes reside, along with one per each [OCI availability domain](https://docs.oracle.com/en-us/iaas/Content/General/Concepts/regions.htm) for OKE. We currently have a single tablet instance in each cell, giving us 6 total replicas.

![Vitess topology](docs/images/multi_dc_vitess.png)

[Apache ZooKeeper](https://zookeeper.apache.org/) is used as the global topology service, as well as a local topo service for each cell. The ZooKeeper instance is deployed alongside Pulsar.

## Connect to Vitess/MySQL

Port forward `vtgate` and `vtctld`

```
kubectl -n vitess port-forward \
  --address localhost "$(kubectl -n vitess get service -l 'planetscale.com/component=vtctld' -o name | head -n1)" \
  15999:15999
```

```
kubectl -n vitess port-forward \
  --address localhost "$(kubectl -n vitess get service -l 'planetscale.com/component=vtgate' -o name | head -n1)" \
  15000:15000
```

Port forward `mysql`:

```
kubectl -n vitess port-forward \
  --address localhost $(kubectl -n vitess get service -l 'planetscale.com/component=vtgate,!planetscale.com/cell' -o name | head -n1) \
  3306:3306
```

You can now access the UI for `vtctld` [here](http://localhost:15000).

For command line access:

```
alias vtctlclient="vtctlclient --server localhost:15999 --alsologtostderr"
alias mysql="mysql -u user -h 127.0.0.1 -p"
```

## Connect to ZooKeeper

Port-forward `zookeeper`:

```
kubectl -n pulsar port-forward \
  --address localhost svc/pulsar-zookeeper \
  2181:2181
```

Download [ZooKeeper](https://zookeeper.apache.org/releases.html). The latest release may be sufficient, but you can also coordinate this with the deployed release if you have issues (check the image tag for ZooKeeper in the `pulsar` namespace).

Run `bin/zkCli.sh` from within the root of the extracted tarball. It should attempt to connect to localhost by default.

## Setting up schemas

```
vtctlclient ApplySchema --sql="$(cat schemas/minds.sql)" minds
vtctlclient ApplyVSchema --vschema="$(cat schemas/minds-vschema.json)" minds
```

## Deploy Helm Chart
 
```
helm upgrade --create-namespace --install vitess . -n vitess
```
